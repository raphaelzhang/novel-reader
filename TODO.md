* GUI? build an executable?
* save history, including xpath for extraction result & jsed result
* more meta info(author, main character, category, other keywords), reading list, search for novels, multi-site download & merge for one novel and recommendation should be supported
* better & more support for output format & control:
	* PDF/ePub/CHM
	* PDF with different resolution (crop & combine pics)
	* customizable charset/encoding for documents & styles(e.g, more CSS for html)
	* single HTML doc
	* port to other devices such as iphone, android, kindle
* better algorithm for web 2.0 pages like douban & dianping and better algorithm for OCR
* IE local file js warning
* performance