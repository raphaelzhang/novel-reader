import lxml.html

from utils import encode_string


def get_ads(samples):
    ads_elements = []
    ads = []
    doms = [lxml.html.fromstring(_.decode('utf-8')) for _ in samples]
    shortest = reduce(lambda r, x: x if r is None or len(r.text_content()) > len(x.text_content()) else r, doms)
    others = set(doms) - {shortest}
    for ele in [_ for _ in shortest.xpath('.//*') if 8 <= len(_.text_content()) < 512]:
        if all(has_element(ele, o) for o in others) and not any(ele in y.xpath('.//*') for y in ads_elements):
            ele.tail = ''  # for print's sake
            ads_elements.append(ele)
            ads.append((ele.tag, ele.text_content().replace('\n', '').replace('\r', ''), None))

    for ele in [_ for _ in shortest.xpath('.//*') if
                _.tail is not None and len(_.text_content()) + len(_.tail) >= 8 and len(_.text_content()) + len(
                        _.tail) < 128]:
        if ele.tag == 'br' and ele.tail.startswith('\r') and ele.tail.count(u'\u2026') > 0:
            continue
        if all(has_element(ele, o, True) for o in others) and not any(ele in y.xpath('.//*') for y in ads_elements):
            ads.append((ele.tag, ele.text_content().replace('\n', '').replace('\r', ''), ele.tail))
            ads_elements.append(ele)

    ads_htmls = [lxml.html.tostring(_, encoding='utf8').decode('utf-8') for _ in ads_elements]
    print ''
    for ad in ads_htmls:
        print 'ads detected[{0}]'.format(len(ad)), encode_string(ad.replace('\r', '').replace('\n', '').strip())
    print ''
    return ads


def has_element(ele, dom, with_tail=False):
    for x in [_ for _ in dom.xpath('.//' + ele.tag) if
        _.text_content().replace('\n', '').replace('\r', '') == ele.text_content().replace('\n', '').replace('\r', '')]:
        if ele.tail == x.tail if with_tail else True:
            return True
    return False


def filter_ads(dom, ads):
    ads_elements = []
    for ad in ads:
        for x in [_ for _ in dom.xpath('.//' + ad[0]) if _.text_content().replace('\n', '').replace('\r', '') == ad[1]]:
            if ad[2] == x.tail if ad[2] is not None else True:
                ads_elements.append(x)
                if ad[2] is not None:
                    x.tail = ''
                break

    for ad in ads_elements:
        ad.drop_tree()
