import os
import datetime

import hashlib
import gzip
import cStringIO
import sqlite3
from urlparse import urlparse


class FsCache(object):
    __time_format = '%Y-%m-%d %H:%M:%S'

    def __init__(self, always_cache=False, path='./.fscache'):
        self.always_cache = always_cache
        self.root = path if path is not None and os.path.isdir(path) else './.fscache'
        self.requests = 0
        self.hits = 0
        self.__init_db()

    def get(self, url, unzip=True):
        self.requests += 1
        sql_url = url.replace("'", "''")
        conn = self.__get_conn()
        row = conn.execute(u"SELECT expire, encoding, path FROM urlcache WHERE url = '{0}'".format(sql_url)).fetchone()
        if row is None:
            conn.close()
            return None

        expire = datetime.datetime.strptime(row[0], self.__time_format)
        path = self.root + '/' + row[2]
        if (not self.always_cache and expire < datetime.datetime.now()) or not os.path.isfile(path):
            self.__remove_entry(url)
            return None
        else:
            f = open(path, 'rb')
            data = f.read()
            f.close()
            if unzip and data[:6] == '\x1f\x8b\x08\x00\x00\x00':
                data = gzip.GzipFile(fileobj=cStringIO.StringIO(data)).read()
            self.hits += 1
            return {'data': data, 'encoding': row[1]}

    def set(self, url, data, expire, encoding=None):
        if expire < datetime.datetime.now() and not self.always_cache:
            self.__remove_entry(url)
            return False

        sql_url = url.replace("'", "''")
        conn = self.__get_conn()
        row = conn.execute(u"SELECT expire, encoding, path FROM urlcache WHERE url = '{0}'".format(sql_url)).fetchone()
        old_expire = datetime.datetime(1900, 1, 1, tzinfo=None) if row is None else datetime.datetime.strptime(row[0], self.__time_format)
        if old_expire >= expire and not (old_expire == expire == datetime.datetime(1900, 1, 1, tzinfo=None)):
            conn.close()
            return False

        path = self.__url2path(url)
        try:
            f = open(self.root + '/' + path, 'wb')
            f.write(data)
            f.close()
            now = datetime.datetime.now().strftime(self.__time_format)
            if row is None:
                conn.execute(u"INSERT INTO urlcache (url, expire, path, updatetime) VALUES('{0}', '{1}', '{2}', '{3}')".
                             format(sql_url, expire.strftime(self.__time_format), path.replace("'", "''"), now))
            else:
                conn.execute(u"UPDATE urlcache SET expire = '{0}', updatetime = '{1}' WHERE url='{2}'".
                             format(expire.strftime(self.__time_format), now, sql_url))
            if encoding is not None:
                conn.execute(u"UPDATE urlcache SET encoding='{0}' WHERE url='{1}'".format(encoding, sql_url))
            conn.commit()
            return True
        except:
            return False
        finally:
            conn.close()

    def remove(self, url):
        self.__remove_entry(url)

    def get_statistics(self):
        conn = self.__get_conn()
        rows = []
        try:
            for r in conn.execute('SELECT url, expire, updatetime FROM urlcache'):
                rows.append({'url': r[0], 'expire': r[1], 'update': r[2]})
        finally:
            conn.close()
        return rows, self.requests, self.hits, self.requests - self.hits

    def __get_conn(self):
        return sqlite3.connect(self.root + '/index')

    def __init_db(self):
        if os.path.isfile(self.root):
            os.remove(self.root)
        if not os.path.isdir(self.root):
            os.makedirs(self.root)

        conn = self.__get_conn()
        conn.execute('CREATE TABLE IF NOT EXISTS urlcache(url TEXT PRIMARY KEY NOT NULL, expire TEXT NOT NULL, encoding TEXT, updatetime TEXT NOT NULL, path TEXT NOT NULL)')
        conn.commit()
        conn.close()

    def __remove_entry(self, url):
        sqlurl = url.replace("'", "''")
        conn = self.__get_conn()
        row = conn.execute(u"SELECT path FROM urlcache WHERE url = '{0}'".format(sqlurl)).fetchone()
        if row is None:
            conn.close()
            return

        path = self.root + '/' + row[0]
        if os.path.isfile(path):
            try:
                os.remove(path)
            except:
                pass
        conn.execute(u"DELETE FROM urlcache WHERE url = '{0}'".format(sqlurl))
        conn.commit()
        conn.close()

    def __url2path(self, url):
        parts = urlparse(url)
        path = parts.netloc + '/' + os.path.dirname(parts.path)
        path = reduce(lambda r, x: r.replace(x, '_'), ':*><?|', path)
        if not os.path.isdir(self.root + '/' + path):
            os.makedirs(self.root + '/' + path)

        names = os.path.splitext(os.path.basename(parts.path))
        name = names[0][:64] + names[1]
        if 0 == len(parts.query) == len(parts.params) == len(parts.fragment) and len(name) > 0:
            return path + '/' + name
        else:
            return path + '/' + hashlib.md5(url.encode('utf8')).hexdigest() + '_' + name
