import sys
import os.path
import urllib2
from urlparse import urljoin
import re
import locale
import datetime
import ConfigParser
import gzip
import cStringIO
import argparse
import shutil

from PIL import Image
import lxml.html

from fscache import FsCache
from async_downloader import AsyncDownloader
from doc_builder import http2local, DocBuilder
from utils import create_request, get_encoding, save_file, encode_string
from extractor import Extractor
from colorful import print_colorful
from resizer import fitwidth
from ads import filter_ads, get_ads


def get_html(url, cache):
    html = None
    if cache is not None:
        cached = cache.get(url)
        if cached is not None:
            html = cached['data']

    if html is None:
        html = urllib2.urlopen(create_request(url), timeout=15).read()
        if cache is not None:
            cache.set(url, html, datetime.datetime.now())

    if html[:6] == '\x1f\x8b\x08\x00\x00\x00':
        html = gzip.GzipFile(fileobj=cStringIO.StringIO(html)).read()

    return html


def get_dom(url, cache):
    html = get_html(url, cache)
    encoding = get_encoding(html)
    dom = lxml.html.fromstring(html.decode(encoding, 'ignore'), base_url=url,
                               parser=lxml.html.HTMLParser(remove_comments=True))
    dom.rewrite_links(lambda l: urljoin(url, l).replace('/../', '/').replace('\n', '').replace('\r', ''))
    return dom


def relax_xpath(path):
    # remove the last [], if any
    return path.find('[') > -1, re.sub(r'\[[^\]]+\]([^\[]*)$', '\g<1>', path)


def get_xpath(ele):
    result = ''
    while ele.getparent() is not None:
        path = ele.tag
        if ele.get('id') is not None:
            path = path + '[@id="' + ele.get('id') + '"]'
        if ele.get('class') is not None:
            path = path + '[@class="' + ele.get('class') + '"]'
        result = '/' + path + result
        ele = ele.getparent()
    return '.' + result


def get_novel_info(dom, cache, excluding=[]):
    titles = dom.xpath('.//title[string-length(text()) > 0]/text()')
    title = titles[0].strip() if len(titles) > 0 else ''
    name = ''.join([_ if _.isalnum() else '|' for _ in title])
    name = name[:name.find('|')]

    # get chapters
    xpaths = [get_xpath(_) for _ in
              dom.xpath('.//a[starts-with(@href, "http://")] | .//a[starts-with(@href, "https://")]')]
    # return (name[:1], xpath[-1])

    max_path = '~'
    for xp in set(xpaths) - set(excluding):
        if xpaths.count(xp) > xpaths.count(max_path):
            max_path = xp

    chapters = [_ for _ in dom.xpath(max_path) if len(_.text_content().strip()) > 0] if max_path != '~' else []
    if len(set([_.text_content() for _ in chapters])) != len(chapters):
        chs = []
        for ch in chapters:
            if ch.text_content() not in [_.text_content() for _ in chs]:
                chs.append(ch)
        chapters = chs

    # get final novel name
    ch = chapters[len(chapters) / 2]
    domch = get_dom(ch.get('href'), cache)
    for i in range(1, len(name)):
        if domch.text_content().count(name[:i + 1]) * 2 <= domch.text_content().count(
                name[:i]) and domch.text_content().count(name[:i + 1]) > 0:
            name = name[:i]
            break

    titles = domch.xpath('.//title[string-length(text()) > 0]/text()')
    if len(chapters) < 100 and (len(titles) == 0 or titles[0].strip().count(name) == 0):
        return get_novel_info(dom, cache, excluding=excluding + [max_path])

    return name, chapters


class ProgressListener(object):
    def __init__(self, width):
        self.output = False
        self.width = width

    def set_output(self, output):
        self.output = output

    def report(self, tname, result, left, url, path, error=None):
        if self.width is not None and result is not None and result:
            fitwidth(path, path, int(self.width))

        if not self.output: return

        print ' ', datetime.datetime.now().strftime('%H:%M:%S') + '/' + tname,
        if result is None:
            print encode_string(u'prepare {0}, with {1} left'.format(url, left))
        elif result:
            print encode_string(u'{0} done, with {1} left'.format(path, left))
        else:
            print encode_string(
                u'error saving {0} to {1} error, with {2} left & error {3}'.format(url, path, left, error.__str__()))


class NovelDb(object):
    __time_format = '%Y-%m-%d %H:%M:%S'

    def __init__(self):
        self.config = ConfigParser.SafeConfigParser()
        self.config.read(['.noveldb'])

    def __commit(self):
        self.config.write(open('.noveldb', 'wb'))

    def update(self, url, name, chapters, execjs):
        section = 'novel ' + url
        if not self.config.has_section(section):
            self.config.add_section(section)

        self.config.set(section, 'name', name.encode('utf8'))
        self.config.set(section, 'chapters', str(chapters))
        self.config.set(section, 'execjs', str(execjs))
        self.config.set(section, 'updatetime', datetime.datetime.now().strftime(self.__time_format))
        self.__commit()

    def get_all(self):
        val = lambda k: self.config.get(n, k) if self.config.has_option(n, k) else ''
        for n in [_ for _ in self.config.sections() if _.startswith('novel ')]:
            yield [val('name').decode('utf8'), val('chapters'), n[6:], val('localtoc').decode('utf8'),
                   val('updatetime')]

    def get_url_by_name(self, name):
        for n in [_ for _ in self.config.sections() if _.startswith('novel ')]:
            if self.config.get(n, 'name').decode('utf8').startswith(name):
                return n[6:]
        return None

    def set_toc(self, url, localtoc):
        section = 'novel ' + url
        if self.config.has_section(section):
            self.config.set(section, 'localtoc', localtoc.encode('utf8'))
            self.config.set(section, 'updatetime', datetime.datetime.now().strftime(self.__time_format))
            self.__commit()

    def get_doc_root(self):
        return self.config.get('config', 'root').decode('utf8') if self.config.has_section(
            'config') and self.config.has_option('config', 'root') else None

    def set_doc_root(self, doc_root):
        if not self.config.has_section('config'):
            self.config.add_section('config')

        self.config.set('config', 'root', doc_root.encode('utf8'))
        self.__commit()

    def rm_cache(self, url):
        section = 'novel ' + url
        if self.config.has_section(section):
            self.config.remove_section(section)
            self.__commit()


class NovelBuilder(DocBuilder):
    __novel_templ_head = u'''\
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>{0}</title>
<style type="text/css" media="all">@import "../img/novel.css";</style>
</head>
<body>
<div id=container>
<div id=header><div class=title>{0}</div>
<div class=nav><a target=_blank href="{1}">{2}</a></div>
</div>
<div id=content_wrap>
<div id=novelnav>
    <a class=toc href="{toc}"></a>
    <a class=prevchapter href="{prev}"></a>
    <a class=nextchapter href="{next}"></a>
    <span>{cur}</span>
    <span>{total}</span>
</div>
<div id=content>
{3}
'''
    # it seems that we cannot use <!-- saved from url=(0014)about:internet --> methods to remove the local IE js waring
    __novel_templ_tail = u'''</div></div></div>
</body>
<script>
document.body.onkeydown = function(ev) {{
    var e = ev || window.event;
    if(e)
    {{
        var key = window.event ? e.keyCode : e.which;
        switch(key) {{
        case 13: //return
            document.location = "{toc}";
            break;
        case 37: //left
            document.location = "{prev}";
            break;
        case 39: //right
            document.location = "{next}"
            break;
        }}
    }}
}};
</script>
</html>'''

    def build_novel_html(self, view_orig='View Original', **kwargs):
        self.build_html_th()

        if self.root is not None:
            indexdir, indexname = http2local(self.url, self.needle)
            for k in kwargs:
                if str(kwargs[k]).startswith('http'):
                    localdir, localname = http2local(kwargs[k], self.needle)
                    kwargs[k] = (('../' + localdir + '/') if indexdir != localdir else '') + localname

        html = self.__novel_templ_head.format(self.title, self.url.encode('utf-8'), view_orig, self.html, **kwargs)
        html += self.build_html_bh()
        html += self.__novel_templ_tail.format(**kwargs)
        self.save_html(html)

        return html


class NovelWriter(object):
    def __init__(self, dom, cache, root, execjs, action, width):
        self.dom = dom
        self.cache = cache
        self.root = root
        self.execjs = execjs
        self.action = action
        self.width = width
        self.db = NovelDb()

    def check_page(self, url, local_path, needle, downloader):
        def imgurl2path(img_url):
            imgdir, imgname = http2local(img_url, needle, ('.png'))
            return os.path.join(self.root, imgdir, imgname)

        f = open(local_path)
        html = f.read()
        f.close()
        dom = lxml.html.fromstring(html.decode('utf-8'))
        for img in dom.xpath('.//img'):
            # backward compatibility
            src = img.get('src').replace('/',
                                         os.path.sep)  # if img.get('src').count('%') == 0 else urllib2.unquote(img.get('src').replace('/', os.path.sep)).decode('utf8')
            src = os.path.dirname(os.path.dirname(local_path)) + src[2:] if src.startswith(
                '..' + os.path.sep) else os.path.dirname(local_path) + os.path.sep + src
            if not os.path.isfile(src):
                netsrc = [_.get('src') for _ in get_dom(url, self.cache).xpath('.//img') if
                          imgurl2path(_.get('src')) == src]
                if len(netsrc) > 0:
                    img_dir, img_name = http2local(netsrc[0], needle, ('.png'))
                    downloader.download(netsrc[0], self.dom.base_url, os.path.join(self.root, img_dir), img_name)
            elif self.width is not None:
                image = Image.open(src)
                if image.size[0] != int(self.width):
                    print encode_string(
                        u'  image {0} width {1} but not {2}, converting...'.format(src, image.size[0], self.width))
                    fitwidth(src, src, int(self.width))

    def get_links(self, current, links):
        def create_link(text, href):
            a = lxml.html.HtmlElement()
            a.tag = 'a'
            a.text = text
            a.set('href', href)
            return a

        i = links.index(current)
        result = []
        for a in ([] if i == 0 else links[i - 1:i]) + [self.dom.base_url] + (
        [] if i == len(links) - 1 else links[i + 1:i + 2]):
            if isinstance(a, unicode):  # toc page
                result.append(create_link(u'\u8fd4\u56de\u76ee\u5f55', self.dom.base_url))
            else:
                result.append(create_link(a.text, a.get('href')))
        return result

    def write_html(self):
        old_links = 0xffffff  # very big chapter count
        if self.action == 'default' and self.cache.get(self.dom.base_url) is not None:
            _, links = get_novel_info(get_dom(self.dom.base_url, self.cache), cache)
            old_links = len(links)

        listener = ProgressListener(self.width)
        downloader = AsyncDownloader(self.cache, listener)
        name, links = get_novel_info(self.dom, self.cache)
        needle = name + '_' if self.width is None else name + '_w' + str(self.width) + '_'
        self.db.update(self.dom.base_url, name, len(links), exec_js)
        print name, len(links), 'Chapters'

        samples = [Extractor(l.get('href'), cache=self.cache).get_text(self.execjs)['html'] for l in links[:3]]
        ads = get_ads(samples)

        regex = re.compile(r'&amp;[a-z]{2,4};')
        for l in links:
            if self.action == 'force':
                self.cache.remove(l.get('href'))

            localdir, localname = http2local(l.get('href'), needle)
            localpath = os.path.join(self.root, localdir, localname)

            # we should remove the old last chapter, because the nextchapter link is wrong
            if self.action == 'default' and old_links == links.index(l) + 1 and os.path.isfile(localpath):
                os.remove(localpath)

            title = name + ' ' + l.text_content()
            if self.action in ['default', 'check'] and os.path.isfile(localpath):
                self.check_page(l.get('href'), localpath, needle, downloader)
                print encode_string(u'{0}/{1} checked.. {2}/{3}'.format(links.index(l) + 1, len(links), localname, title))
                continue

            while True:
                print encode_string(u'{0}/{1} fetching.. {2}/{3}'.format(links.index(l) + 1, len(links), localname, title))
                text = Extractor(l.get('href'), cache=self.cache).get_text(self.execjs)
                if text['success']:  # if error, continue fetching
                    break

            pagedom = lxml.html.fromstring(text['html'].decode('utf-8'))
            filter_ads(pagedom, ads)

            text['html'] = regex.sub('', lxml.html.tostring(pagedom, encoding='utf-8').decode('utf-8')).encode('utf8')

            builder = NovelBuilder(l.get('href'), title, text['html'], self.get_links(l, links), self.root, needle,
                                   downloader)
            builder.build_novel_html('View Original',
                                     toc=self.dom.base_url,
                                     next=self.dom.base_url if links.index(l) + 1 == len(links) else links[
                                       links.index(l) + 1].get('href'),
                                     prev=self.dom.base_url if links.index(l) == 0 else links[links.index(l) - 1].get(
                                       'href'),
                                     cur=links.index(l) + 1,
                                     total=len(links)
                                     )
            print encode_string(u'{0}/{1} processed.. {2}/{3}'.format(links.index(l) + 1, len(links), localname, title))

        locallinks = [(_.text, http2local(_.get('href'), needle)) for _ in links]
        # in case the index page is not in TOC list & thus cannot get refreshed
        self.write_toc(self.dom.base_url, name, locallinks, downloader)

        listener.set_output(True)
        print 'shutting down...(press Ctrl-C to force shutdown)'
        try:
            downloader.shutdown()
        except:  # if user sends Ctrl-C or something like that
            downloader.shutdown(False)

        tocdir, tocname = http2local(self.dom.base_url, needle)
        print 'Local TOC:', encode_string(os.path.join(self.root, tocdir, tocname))

    def write_toc(self, toc, name, local_links, downloader):
        needle = name + '_' if self.width is None else name + '_w' + str(self.width) + '_'
        # remove the old toc page from cache
        if self.action != 'check':
            self.cache.remove(toc)

        text = Extractor(toc, cache=self.cache).get_text(self.execjs)
        html = text['html'].decode('utf-8')

        mydir, myname = http2local(toc, needle)
        html += u'<table width="100%">'
        for i in range(len(local_links) / 3 + 1):
            html += u'<tr>'
            for l in local_links[i * 3:i * 3 + 3]:
                html += u'<td width="33%"><a href="{0}">{1}</a></td>'.format(
                    (('../' + l[1][0] + '/') if mydir != l[1][0] else '') + l[1][1], l[0])
            html += u'</tr>'
        html += u'</table>'

        title = u'{0} / {1} Chs'.format(name, len(local_links))
        builder = NovelBuilder(toc, title, html.encode('utf-8'), [], self.root, needle, downloader)
        builder.build_html()
        self.db.set_toc(self.dom.base_url, os.path.join(self.root, mydir, myname))

    def write_text(self):
        name, links = get_novel_info(self.dom, self.cache)
        self.db.update(self.dom.base_url, name, len(links), exec_js)
        print name, len(links), 'Chapters'

        samples = [Extractor(l.get('href'), cache=self.cache).get_text(self.execjs)['html'] for l in links[:3]]
        ads = get_ads(samples)

        regex = re.compile(r'&amp;[a-z]{2,4};')
        whole = ''
        for l in links:
            title = name + ' ' + l.text_content()
            while True:
                print encode_string(u'{0}/{1} fetching.. {2}'.format(links.index(l) + 1, len(links), title))
                text = Extractor(l.get('href'), cache=self.cache).get_text(self.execjs)
                if text['success']:  # if error, continue fetching
                    break

            text['html'] = regex.sub('', text['html'].decode('utf-8')).encode('utf8')
            page_dom = lxml.html.fromstring(text['html'].decode('utf-8'))
            filter_ads(page_dom, ads)

            for p in page_dom.xpath('.//br | .//p'):
                p.tail = '\n' if p.tail is None else p.tail + '\n'

            whole += '=' * 10 + ' ' + l.text_content() + ' ' + '=' * 10 + '\n'
            whole += page_dom.text_content() + '\n\n'

            print encode_string(u'{0}/{1} processed.. {2}'.format(links.index(l) + 1, len(links), title))

        save_file(self.root, name + '.txt', whole.encode('gbk', 'ignore'))
        print 'Local Novel:', encode_string(self.root + '/' + name + '.txt')


def rm_cache(dom, cache):
    def rm_doc(link):
        data = cache.get(link.get('href'))
        if data is None:
            return
        encoding = get_encoding(data['data'])
        if encoding is not None:  # html not empty
            mydom = lxml.html.fromstring(data['data'].decode(encoding, 'ignore'))
            mydom.rewrite_links(lambda l: urljoin(link.get('href'), l))
            [cache.remove(res.get('src')) for res in mydom.xpath('.//script[@src] | .//img[@src]')]
        print encode_string(u'removing {0}/{1}'.format(link.text_content(), link.get('href')))
        cache.remove(link.get('href'))

    name, links = get_novel_info(dom, cache)
    inp = raw_input(encode_string(u'Are you sure to remove {0} ({1} Chapters) from cache? (y/n)'.format(name, len(links))))
    if inp.lower() == 'y':
        [rm_doc(l) for l in links]
        print encode_string(u'removing root: {0}'.format(dom.base_url))
        cache.remove(dom.base_url)
        NovelDb().rm_cache(dom.base_url)


def init_root(root_folder):
    if not os.path.isdir(root_folder):
        os.makedirs(root_folder)

    if not os.path.exists(os.path.join(root_folder, 'img')) and os.path.isdir(os.path.join('.', 'img')):
        shutil.copytree(os.path.join('.', 'img'), os.path.join(root_folder, 'img'))

    print 'root folder:', root_folder


def get_args():
    root_folder = NovelDb().get_doc_root()
    if root_folder is None:
        root_folder = './.offline_novels'

    parser = argparse.ArgumentParser(description="Download novels from index page")
    parser.add_argument('--url', '-u', dest='url', help="novel index page url")
    parser.add_argument('--root', '-r', dest='root', default=root_folder, help='root folder of the offline docs (default: %(default)s)')
    parser.add_argument('--action', '-a', dest='action', default='default', choices=['default', 'force', 'check', 'rmcache'], help='refresh action (default: %(default)s)')
    parser.add_argument('--execjs', '-j', dest='execjs', action='store_true', help='execute javascript in the page (needs Windows or PyV8)')
    parser.add_argument('--width', '-w', dest='width', default=-1, type=int, help='set the width of the image(if any) in [100, 10240]')
    parser.add_argument('--target', '-t', dest='target', default='html', choices=['html', 'text'], help='save content as html or text (default: %(default)s)')

    args = parser.parse_args()

    NovelDb().set_doc_root(args.root)

    if args.width != -1 and (args.width < 100 or args.width > 10240):
        args.width = -1

    return args.root, args.url, args.target, args.action, args.execjs, args.width


def list_all(cache):
    count = 1
    for novel in NovelDb().get_all():
        novel[1] = int(novel[1])  # chapter from str -> int
        print_colorful('brown', '=' * 35, count, '=' * 35)
        print 'Name:\t\t', encode_string(novel[0]), '({0} Chapters, {1})'.format(novel[1], novel[4])
        print 'URL:\t\t', encode_string(novel[2])
        print 'Local TOC:\t', encode_string(novel[3])
        count += 1

        dom = get_dom(novel[2], cache)
        if dom is None:
            continue

        name, links = get_novel_info(dom, cache)
        print 'Last Chapter:\t', encode_string(links[-1].text_content()),
        try:
            name, links = get_novel_info(get_dom(novel[2], None), cache)
            if len(links) > novel[1]:
                print_colorful('red', '+{0}'.format(len(links) - novel[1]), nocrlf=True)
                print_colorful('green', encode_string(links[-1].text_content()))
            else:
                print
        except Exception, e:
            print >> sys.stderr, '\nerror getting dom:', e.__str__()


if __name__ == '__main__':
    root_folder, url, target, action, exec_js, width = get_args()

    cache = FsCache(always_cache=True, path='./.novel')

    if url is None:
        list_all(cache)
        exit()

    # if url isn't http..., we guess it's a novel name
    if not url.lower().startswith(("http://", "https://")):
        name = url.decode(locale.getdefaultlocale()[1], 'ignore')
        url = NovelDb().get_url_by_name(name)
        if url is None:
            print >> sys.stderr, encode_string(u"There's no novel named {0}, so quit".format(name))
            exit(1)

    if action == 'default' and target == 'html' and cache.get(url) is not None:
        name, links_old = get_novel_info(get_dom(url, cache), cache)
        name, links_new = get_novel_info(get_dom(url, None), cache)
        if len(links_new) == len(links_old) and all(
                (lxml.html.tostring(_[0]) == lxml.html.tostring(_[1]) for _ in zip(links_old, links_new))):
            print u'Novel {0}({1}) has no change, so quit'.format(name, url)
            exit()

    dom = get_dom(url, cache if action in ['check', 'rmcache'] else None)
    print 'homepage loaded..'

    if action == 'rmcache':
        rm_cache(dom, cache)
        exit()

    init_root(root_folder)

    writer = NovelWriter(dom, cache, root_folder, exec_js, action, width)
    apply(getattr(writer, 'write_' + target))
