import sys
import os
import cProfile
import pstats

from PIL import Image

from resizer import parsewords, parselines, fitwidth


def parseimgwords(infile):
    img1 = Image.open(infile) if isinstance(infile, str) else infile
    # width, height = img1.size
    # if isinstance(infile, str):
    #     print width, height

    # for img1: 255 means white & 0 means black
    # we will have to set 1 to mean black & 0 to mean white
    rawdata = list(img1.getdata())

    gaps = parsewords(rawdata, img1.size[0], 0, img1.size[1])

    return {'img1': img1, 'gaps': gaps}


def analyseline(infile, outpath):
    wordsinfo = parseimgwords(infile)
    img1 = wordsinfo['img1']
    gaps = wordsinfo['gaps']
    # print 'gaps', len(gaps), gaps

    # exit()
    for g in gaps:
        imgword = img1.crop((g[0], 0, g[0] + g[1], img1.size[1]))
        if not os.path.isdir(outpath):
            os.makedirs(outpath)
        if isinstance(infile, str):
            print g, (g[0], 0, g[0] + g[1], img1.size[1]), imgword.size
            imgword.show()
        else:
            imgword.save(os.path.join(outpath, str(g[0]) + ".png"))


def savepara(imgline, vspace, top, cparas, mydir, expecting_para_start, paradir, suffix):
    width, lheight = imgline.size
    leading_raw = sum([1 - (_ & 1) for _ in list(imgline.crop((0, 0, lheight*2 - 2*vspace, lheight)).getdata())])
    if leading_raw == 0 or cparas == 1:  # this is a para start, for the first sep, it maybe that ads above is too much
        paradir = os.path.join(mydir, str(cparas) + '_para')
        cparas += 1
    elif leading_raw > 0 and expecting_para_start:  # this is a separator or ad
        paradir = os.path.join(mydir, str(cparas) + '_sep')
        cparas += 1
    else:  # a para content, so skip
        pass

    if paradir != '' and not os.path.isdir(paradir):
        os.makedirs(paradir)

    imgline.save(os.path.join(paradir, str(top)) + suffix)

    ending_raw = sum([1 - (_ & 1) for _ in list(imgline.crop((width - 2*lheight + 2*vspace, 0, width, lheight)).getdata())])
    if ending_raw == 0 or (leading_raw > 0 and expecting_para_start and cparas > 2):
        expecting_para_start = True
    else:
        expecting_para_start = False

    return cparas, expecting_para_start, paradir


def analysepage(infile, outpath):
    linesinfo = parselines(infile)
    img1 = linesinfo['img1']
    gaps = linesinfo['gaps']
    cheight = linesinfo['cheight']
    vspace = linesinfo['vspace']
    print len(gaps), cheight, vspace

    expecting_para_start = True
    cparas = 1
    paradir = ''
    for g in gaps:
        # print g
        imgline = img1.crop((0, g[0], img1.size[0], g[1]))
        cparas, expecting_para_start, paradir = savepara(imgline, vspace, g[0], cparas, outpath, expecting_para_start, paradir, '.png')
        analyseline(imgline, os.path.join(paradir, str(g[0])))


def test():
    infiles = [
        './samples/1.jpg',
        './samples/2.gif',
        './samples/3.gif',
        './samples/4.gif',
        './samples/5.gif'
    ]

    if len(sys.argv) > 1:
        infiles = sys.argv[1:]

    for infile in infiles:
        print 'processing {0}/{1}: {2}'.format(infiles.index(infile) + 1, len(infiles), infile)
        # outdir = './samples/out/{0}_{1}.paras'.format(infiles.index(infile) + 1, os.path.basename(infile))
        outfile = './samples/out/{0}_{1}.png'.format(infiles.index(infile) + 1, os.path.basename(infile))
        # analyseline(infile, outfile)
        fitwidth(infile, outfile, 480)
        # analysepage(infile, outdir)


if __name__ == '__main__':
    prof = cProfile.Profile()
    prof.enable()
    test()
    prof.disable()
    pstats.Stats(prof).strip_dirs().sort_stats('cumulative').print_stats(25)