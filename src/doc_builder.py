import sys
import os
import hashlib
import urllib2
import locale
from urlparse import urlparse

import lxml.html

from utils import save_file


def http2local(url, needle='', suffix=('.html', '.htm')):
    parts = urlparse(url)
    name = parts.path[1:] if parts.path.startswith('/') else parts.path
    if len(parts.query) == len(parts.params) == 0:
        name = name.replace('/', '_').lower()
        if not name.endswith(suffix):
            name += suffix[0] if isinstance(suffix, tuple) else suffix
    else:
        name = name.replace('/', '_').lower() + '_' + hashlib.md5(url.encode('utf8')).hexdigest() + suffix[0]
    return needle + parts.netloc.replace(':', '_').lower(), name


class DocBuilder(object):
    __html_templ_head = u'''\
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>{0}</title>
<style type="text/css" media="all">@import "../img/common.css";</style>
<style type="text/css" media="print">@import "../img/print.css";</style>
</head>
<body>
<div id=container>
<div id=header><div class=title>{0}</div>
<div class=nav><a target=_blank href="{1}">{2}</a> | <a href="javascript:window.print()">{3}</a></div>
</div>
<div id=content>
{4}
</div>
'''

    __html_templ_tail = u'''</div>
</body>
</html>'''

    def __init__(self, url, title, html, siblings, local_root=None, needle='', downloader=None):
        self.url = url
        self.title = title
        self.html = html.decode('utf-8')
        self.links = siblings
        self.needle = needle
        self.downloader = downloader
        self.root = local_root if local_root is not None and os.path.isdir(local_root) else None

    def build_html(self, view_orig='View Original', print_page='Print Page'):
        self.build_html_th()

        html = self.__html_templ_head.format(self.title, self.url.encode('utf-8'), view_orig, print_page, self.html)
        html += self.build_html_bh()
        html += self.__html_templ_tail
        self.save_html(html)

        return html

    # build html-top-half
    def build_html_th(self):
        # turn every image into local image
        if self.root is None:
            return

        ele = lxml.html.fromstring(self.html)
        self.__save_imgs(ele)
        index_dir, index_name = http2local(self.url, self.needle)
        for sib in self.links:
            local_dir, local_name = http2local(sib.get('href'), self.needle)
            sib.set('href', (('../' + local_dir + '/') if index_dir != local_dir else '') + local_name)
            self.__save_imgs(sib)
        self.html = lxml.html.tostring(ele, encoding='utf-8').decode('utf-8')

    # build html-bottom-half
    def build_html_bh(self):
        if len(self.links) == 0:
            return ''
        html = "<div id=footer>"
        for a in self.links:
            if self.root is None:
                a.set('target', '_blank')
            html += lxml.html.tostring(a, encoding='utf-8').decode('utf-8')
        html += "</div>\n"
        return html

    def save_html(self, html):
        if self.root is None:
            return
        local_path, local_name = http2local(self.url, self.needle)
        try:
            # hack to solve the IE path encoding problem
            quoted = '="../' + urllib2.quote(self.needle.encode('utf8'))
            html = html.encode('utf-8').replace(quoted, '="../' + self.needle.encode('utf8'))
            save_file(os.path.join(self.root, local_path), local_name, html)
        except Exception, e:
            print >> sys.stderr, 'error saving {0} to {1}, msg: {2}'.format(self.url,
                            os.path.join(self.root, local_path, local_name).encode(locale.getdefaultlocale()[1], 'ignore'), e.__str__())

    def __save_imgs(self, ele):
        if self.downloader is None:
            return
        # escape = lambda s: ''.join([_ if ord(_) < 127 else '&#' + str(ord(_)) + ';' for _ in s])

        index_dir, index_name = http2local(self.url, self.needle)
        for img in ele.xpath('.//img[starts-with(@src, "http://")] | .//img[starts-with(@src, "https://")]'):
            # we use only png images to facilitate later image width conversion
            local_dir, local_name = http2local(img.get('src'), self.needle, ('.png'))
            self.downloader.download(img.get('src'), self.url, os.path.join(self.root, local_dir), local_name)
            # img.set('src', (('../' + escape(local_dir) + '/') if index_dir != local_dir else '') + local_name)
            img.set('src', (('../' + local_dir + '/') if index_dir != local_dir else '') + local_name)
