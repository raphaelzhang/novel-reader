# -*- coding: utf-8 -*-

import sys
import os
import re
from urlparse import urlparse
import cProfile
import pstats

import lxml.html

from fscache import FsCache
from doc_builder import DocBuilder
from extractor import Extractor
from ads import filter_ads, get_ads


def test(cache):
    if len(sys.argv) == 1:
        print >> sys.stderr, 'Please input url'
        sys.exit(1)

    if not os.path.exists('./hte'):
        os.makedirs('./hte')

    parts = urlparse(sys.argv[1])
    text = Extractor(sys.argv[1], cache=cache).get_text(False)
    links = [sys.argv[1]] + [_.get('href') for _ in text['links']]

    samples = [Extractor(l, cache=cache).get_text(False)['html'] for l in links[:3]]
    ads = get_ads(samples)
    regex = re.compile(r'&amp;[a-z]{2,4};')

    whole = ''
    for l in links:
        text = Extractor(l, cache=cache).get_text(False)
        pagedom = lxml.html.fromstring(text['html'].decode('utf-8'))
        filter_ads(pagedom, ads)
        whole += regex.sub('', lxml.html.tostring(pagedom, encoding='utf-8').decode('utf-8')).encode('utf8')

    builder = DocBuilder(sys.argv[1], text['title'], whole, [])
    html = builder.build_html(u'浏览原网页', u'打印')
    f = open('./hte/c_{0}.html'.format(parts.netloc), 'wb')
    f.write(html.encode('utf-8'))
    f.close()


if __name__ == "__main__":
    cache = FsCache(always_cache=True)
    prof = cProfile.Profile()
    prof.enable()
    test(cache)
    prof.disable()
    pstats.Stats(prof).strip_dirs().sort_stats('cumulative').print_stats(25)
