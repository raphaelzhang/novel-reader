import sys
from PIL import Image


# TODO: better remove-ads algorithm
def rle(stats):
    rled = []

    head = tail = -2
    for i in stats:
        if i == tail + 1:
            tail = i
        else:
            if head >= 0:
                rled.append((head, tail - head + 1))
            head = tail = i

    if tail > head:
        rled.append((head, tail-head+1))
    return rled


def combine(gaps, threshold, above_threshold = True):
    result = []
    for g in gaps:
        if g[1] > threshold:
            result.append(g)
            continue
        elif len(result) > 0 and result[-1][1] + result[-1][0] > g[0]:
            continue

        start = g
        current = g
        while gaps.index(current) < len(gaps):
            if current[0] + current[1] - start[0] > threshold:
                if not above_threshold:
                    current = gaps[gaps.index(current) - 1]
                result.append((start[0], current[0] + current[1] - start[0]))
                break

            if gaps.index(current) == len(gaps) - 1:
                if not above_threshold:
                    if current != start:
                        current = gaps[gaps.index(current) - 1]
                    result.append((start[0], current[0] + current[1] - start[0]))
                break

            current = gaps[gaps.index(current) + 1]
            if current[1] > threshold:
                if not above_threshold:
                    current = gaps[gaps.index(current) - 1]
                    result.append((start[0], current[0] + current[1] - start[0]))
                break

    return result


def hrline(rawdata, width, startline, endline):
    lines = []
    for i in range(startline, endline):
        blacks = rle([_ for _ in range(width) if rawdata[i*width + _] == 0])
        lines.append(max([_[1] for _ in blacks]) if len(blacks) > 0 else 0)

    hrlines = [_ for _ in range(startline, endline) if lines[_ - startline] > 60]
    if len(hrlines) > 0:
        # print hrlines
        # print 'prev startline', startline, 'new startline', hrlines[len(hrlines) - 1]
        return hrlines[len(hrlines) - 1]
    return startline - 1


def parselines(infile):
    img = Image.open(infile)
    img1 = img.convert('1')
    width, height = img1.size
    # for img1: 255 means white & 0 means black
    rawdata = list(img1.getdata())

    lines = [sum(rawdata[i*width:(i+1)*width]) for i in range(height)]
    threshold = (width - 3)*0xff
    if len([_ for _ in lines if _ <= threshold]) > (height*3/4):
        #there are too many noise in the converted image, so correct it
        for j in range(1, height - 1):
            for i in range(1, width - 1):
                pos = j*width + i
                if rawdata[pos] == 0 and rawdata[pos - 1] == 0xff and rawdata[pos + 1] == 0xff:
                    rawdata[pos] = 0xff
        lines = [sum(rawdata[i*width:(i+1)*width]) for i in range(height)]
        if len([_ for _ in lines if _ <= threshold]) > (height*3/4):
            threshold = (min(lines) + 3*max(lines)) >> 2

    blacklines = [_ for _ in range(height) if lines[_] <= threshold]

    # print 'blacklines', len(blacklines), blacklines
    # print 'rle(blacklines)', len(rle(blacklines)), rle(blacklines)
    rled_blacklines = [_ for _ in rle(blacklines) if _[1] < 35]  # and (_[1] > [5] or hrline(rawdata, width, _[0], _[0] + _[1]) == _[0] - 1)]
    # print 'rled_blacklines', len(rled_blacklines), rled_blacklines
    vspace = 2
    cheight = max([_[1] for _ in rled_blacklines])
    # print cheight, vspace, set([_[1] for _ in rled_blacklines])

    gaps = combine([_ for _ in rled_blacklines if _[1] < cheight*3/2], cheight/2)
    cheight = sum([_[1] for _ in gaps])/len(gaps)
    # print 'gaps', len(gaps), gaps, cheight
    gaps2 = []
    for g in gaps:
        top = g[0] - (cheight - g[1])/2 - vspace
        bottom = top + cheight + 2*vspace

        # avoid the <hr> tragedy
        # remove the <hr> first, if any
        if img1.size[1] > bottom > top >= 0:
            sl = hrline(rawdata, width, top, bottom)
            if sl >= top:
                top = sl + 1
                bottom = top + cheight + 2*vspace

        if 0 < gaps.index(g) < len(gaps) - 1:
            nextg = gaps[gaps.index(g) + 1]
            lastg = gaps[gaps.index(g) - 1]
            if not (nextg[0] > bottom > top > lastg[0] + lastg[1]) and (nextg[0] - lastg[0] - lastg[1] > cheight + 2*vspace):
                top = g[0] - vspace
                bottom = top + cheight + 2*vspace
                if bottom > nextg[0]:
                    bottom = nextg[0] - vspace
                    top = bottom - cheight - 2*vspace

        if img1.size[1] > bottom > top >= 0:
            gaps2.append((top, bottom))

    # print 'gaps2', len(gaps2), gaps2
    return {'img': img, 'img1': img1, 'gaps': gaps2, 'cheight': cheight, 'vspace': vspace, 'rawdata': rawdata}


def parsewords(rawdata, width, startline, endline):
    # avoid the <hr> tragedy
    # remove the <hr> first, if any
    startline = hrline(rawdata, width, startline, endline) + 1

    # cols = [sum([rawdata[_*width + i] for _ in range(startline, endline)]) for i in range(width)]
    # threshold = (min(cols) + 4*max(cols))/5
    # print len(cols), min(cols), max(cols), len([_ for _ in cols if _ < (min(cols) + 3*max(cols))/4])

    # blackcols = [i for i in range(width) if cols[i] < threshold]
    blackcols = [i for i in range(width) if any([rawdata[_*width + i] == 0 for _ in range(startline, endline)])]

    # if isinstance(infile, str):
    #     print 'blackcols', len(blackcols), blackcols

    rled_blackcols = [_ for _ in rle(blackcols) if _[1] > 1]
    # if isinstance(infile, str):
    #     print 'rled_blackcols', rled_blackcols, len(rled_blackcols)

    # combine all the small pieces together for the sake of ... & xiao(3) & chuan(1), etc.
    gaps = combine(rled_blackcols, (endline - startline)*2/3, False)

    # if isinstance(infile, str):
    #     print gaps, len(gaps)
    return [(_[0]-2, _[1]+4) for _ in gaps]


def fillline(x, y, gaps, imgline, newimage, cheight, vspace, hmargin, paramargin):
    if gaps[0][0] > 3*cheight/2:  # read new para
        if x > hmargin:  # but not write new para
            y += 2*vspace + cheight + 4
        x = hmargin + paramargin

    curword = 0
    while curword < len(gaps):
        endword = curword
        while endword < len(gaps):
            w = gaps[endword][0] + gaps[endword][1] - gaps[curword][0]
            if x + w + hmargin > newimage.size[0]:
                break
            endword += 1
        endword -= 1
        tmpimage = imgline.crop((gaps[curword][0], 0, gaps[endword][0] + gaps[endword][1], imgline.size[1]))
        # print curword, endword, gaps[curword], gaps[endword], x, y, tmpimage.size, w

        # if we use RGBA, we can use tmpimage as the mask to make background transparent
        # otherwise we should use newimage.paste(tmpimage, (x, y))
        newimage.paste(tmpimage, (x, y), tmpimage)
        x += tmpimage.size[0]
        curword = endword + 1

        if curword >= len(gaps):
            break

        # should we wrap to the next line?
        if x + gaps[curword][1] + hmargin > newimage.size[0]:
            y += 2*vspace + cheight + 4
            x = hmargin

    return x, y


def fitwidth(infile, outfile, width, vmargin=15, hmargin=5, paramargin=40):
    if width < 100 or width > 10240:
        print >> sys.stderr, 'width too big or too small:', width
        sys.exit(1)

    linesinfo = parselines(infile)
    img = linesinfo['img'].convert('RGBA')
    img1 = linesinfo['img1']
    lines = linesinfo['gaps']
    cheight = linesinfo['cheight']
    vspace = linesinfo['vspace']
    rawdata = linesinfo['rawdata']

    newimage = Image.new('RGBA', (width, img1.size[0]*img1.size[1]*6/width/5), (0, 0, 0, 0))
    # newimage = Image.new('1', (width, img1.size[0]*img1.size[1]*6/width/5), 255)
    # print 'new size:', newimage.size, 'orig size:', img1.size
    x, y = hmargin, vmargin
    for l in lines:
        gaps = parsewords(rawdata, img1.size[0], l[0], l[1])
        # print 'line', l
        # print 'gaps', gaps
        if len(gaps) == 0:
            continue

        imgline = img.crop((0, l[0], img1.size[0], l[1]))
        # imgline = img1.crop((0, l[0], img1.size[0], l[1]))

        x, y = fillline(x, y, gaps, imgline, newimage, cheight, vspace, hmargin, paramargin)

        # end of a paragraph, so start with a new line
        if gaps[-1][0] + gaps[-1][1] + 3*cheight/2 < imgline.size[0]:
            if lines.index(l) < len(lines) - 1:
                y += 2*vspace + cheight + 4
                x = hmargin

    y += 2*vspace + cheight + 4
    y += vmargin
    newimage = newimage.crop((0, 0, width, y))

    newimage.save(outfile)