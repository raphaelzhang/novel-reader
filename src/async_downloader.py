import sys
import os
import urllib2

import threading
import time

import gzip
import cStringIO

from utils import create_request, save_file, get_expire


class AsyncDownloader(object):
    __max_threads = 5

    def __init__(self, cache, listener=None):
        self.threads = []
        self.mutex = threading.Lock()
        self.quit = threading.Event()
        self.tasks = []
        self.cache = cache
        self.listener = listener
        self.pendings = 0

    def shutdown(self, wait=True, timeout=None):
        if wait:
            hastasks = True
            while hastasks:
                self.mutex.acquire()
                try:
                    hastasks = len(self.tasks) > 0 or self.pendings > 0
                finally:
                    self.mutex.release()
                if hastasks:
                    time.sleep(0.3)

        self.quit.set()
        for t in self.threads:
            t.join(timeout)

        if not wait and len(self.tasks) > 0:
            print >> sys.stderr, len(self.tasks)
            for t in self.tasks:
                print >> sys.stderr, t[0]

    def download(self, url, referer, folder, name):
        if os.path.isfile(os.path.join(folder, name)):
            return

        self.mutex.acquire()
        try:
            cached = self.cache.get(url)
        finally:
            self.mutex.release()

        try:
            if cached is not None and len(cached['data']) not in [0, 1024]:
                save_file(folder, name, cached['data'])
                if self.listener is not None:
                    self.listener.report('dlX', True, len(self.tasks), url, os.path.join(folder, name), None)
            else:
                self.__addinternal(url, referer, folder, name)
        except:
            pass

    def __addinternal(self, url, referer, folder, name):
        if len(self.threads) < self.__max_threads:
            t = threading.Thread(target=self.__downloader, name='dl' + str(len(self.threads)))
            t.start()
            self.threads.append(t)

        self.mutex.acquire()
        self.tasks.append((url, referer, folder, name))
        self.mutex.release()

    def __downloader(self):
        tname = threading.current_thread().name
        while not self.quit.is_set():
            url = ''
            self.mutex.acquire()
            try:
                if len(self.tasks) > 0:
                    self.pendings += 1
                    url, referer, folder, name = self.tasks.pop(0)
                    if self.listener is not None:
                        self.listener.report(tname, None, len(self.tasks), url, os.path.join(folder, name))
            finally:
                self.mutex.release()

            if len(url) == 0:
                if self.quit.is_set():
                    break
                time.sleep(0.3)
                continue

            error = None
            try:
                # print '  urlopen#1 for', url
                f = urllib2.urlopen(create_request(url, referer), timeout=15)
                data = f.read()
                if data[:6] == '\x1f\x8b\x08\x00\x00\x00':
                    data = gzip.GzipFile(fileobj=cStringIO.StringIO(data)).read()
                save_file(folder, name, data)
            except Exception, e:
                error = e

            self.mutex.acquire()
            try:
                if error is None:
                    self.cache.set(url, data, get_expire(f))
                else:
                    self.tasks.append((url, referer, folder, name))
                if self.listener is not None:
                    self.listener.report(tname, error is None, len(self.tasks), url, os.path.join(folder, name), error)
            finally:
                self.pendings -= 1
                self.mutex.release()
