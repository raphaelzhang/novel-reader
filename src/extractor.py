import sys
import os
import datetime
import urllib2
from urlparse import urlparse, urljoin, parse_qs
import re
import cStringIO
import gzip
import base64
import sqlite3
import shutil

from PIL import Image
import lxml.html

from fscache import FsCache
from utils import create_request, get_expire, get_encoding, js_engine, encode_string


class ExtractStat(object):
    __time_format = '%Y-%m-%d %H:%M:%S'

    def __init__(self, path='./.extractstat'):
        self.path = path if path is not None and os.path.isdir(path) else './.extractstat'
        if os.path.isdir(self.path):
            shutil.rmtree(self.path)

        self.conn = sqlite3.connect(self.path)
        self.conn.execute('CREATE TABLE IF NOT EXISTS urlerror(url TEXT PRIMARY KEY NOT NULL, errors INTEGER NOT NULL, updatetime TEXT NOT NULL)')
        self.conn.commit()

    def __del__(self):
        self.conn.close()

    def add_error_url(self, url):
        sql_url = url.replace("'", "''")
        row = self.conn.execute(u"SELECT errors FROM urlerror WHERE url='{0}'".format(sql_url)).fetchone()
        now = datetime.datetime.now().strftime(self.__time_format)
        if row is None:
            self.conn.execute(u"INSERT INTO urlerror (url, errors, updatetime) VALUES('{0}', 1, '{1}')".format(sql_url, now))
        else:
            self.conn.execute(u"UPDATE urlerror SET errors = {0}, updatetime = '{1}' WHERE url='{2}'".format(row[0]+1, now, sql_url))
        self.conn.commit()

    def rm_error_url(self, url):
        sql_url = url.replace("'", "''")
        self.conn.execute(u"DELETE FROM urlerror WHERE url='{0}'".format(sql_url))
        self.conn.commit()

    def get_error_urls(self):
        result = []
        for row in self.conn.execute("SELECT url, errors, updatetime FROM urlerror").fetchall():
            result.append({'url': row[0], 'errors': row[1], 'time': datetime.datetime.strptime(row[2], self.__time_format)})
        return result


merges = [re.compile(r'&[a-z]{2,4};'), re.compile(r'\d{2,4}\-\d{1,2}\-\d{1,2}( \d{1,2}:\d{1,2})?')]

def too_many_links(e):
    return (int(e.get('_te_textlen')) - int(e.get('_te_purelen'))) * 3 > int(e.get('_te_textlen'))

class Extractor(object):
    '''the real extactor class to do extract, and the main method is get_text'''

    def __init__(self, url, execjs=True, cache=None):
        self.cache = cache if cache is not None else FsCache(always_cache=True)
        self.url = url.strip()
        if not self.url.lower().startswith(("http://", "https://")):
            self.url = "http://" + self.url
        self.stat = ExtractStat()
        self.success = True

    def get_text(self, exec_js, get_siblings=True, debug_callback=None):
        dom, encoding = self.__create_dom()
        if callable(debug_callback):
            debug_callback(0, dom, self.url, encoding)

        self.__prune_dom(dom, exec_js, encoding)
        if callable(debug_callback):
            debug_callback(1, dom, self.url, encoding)

        # get title
        title = dom.xpath('.//title[string-length(text()) > 0]')
        title = title[0].text if len(title) > 0 else ''

        # get sibling links
        siblings = self.__get_siblings(dom) if get_siblings else []
        if callable(debug_callback):
            debug_callback(2, dom, self.url, encoding)

        # build statistics tree
        tags, total = self.__build_stat_tree(dom)
        if callable(debug_callback):
            debug_callback(2, dom, self.url, encoding)

        [self.__remove_element(_) for _ in dom.xpath('.//img') if self.__is_ads(_)]
        if callable(debug_callback):
            debug_callback(3, dom, self.url, encoding)

        root = dom
        bodies = dom.xpath('.//body[@_te_textlen>{0}]'.format(total/2))
        if len(bodies) == 1:
            root = bodies[0]
        if callable(debug_callback):
            debug_callback(4, dom, self.url, encoding)

        # get html
        ele = self.__get_textblock_element(root, siblings)
        if callable(debug_callback):
            debug_callback(5, dom, self.url, encoding)

        for a in siblings:
            self.__remove_attrs(a)
            for c in a.iter():
                self.__remove_attrs(c)

        if ele is not None:
            self.__remove_attrs(ele)
            for c in ele.iter():
                self.__remove_attrs(c)
            html = lxml.html.tostring(ele, encoding='utf-8', pretty_print=True)
        else:
            html = ''

        return {'title': title, 'html': html, 'links': siblings, 'success': self.success}

    def __create_dom(self):
        data = self.__open_url(self.url)
        html = data['data'].replace('\x00', '')

        if 'encoding' in data and data['encoding'] is not None:
            encoding = data['encoding'].lower()
        else:
            encoding = get_encoding(html)
            if encoding is None:
                encoding = 'utf8'

        # remove all </html> tags so if there are more than 1 </html> tags, we can avoid
        # the problem of early content termination
        html = re.sub(r'</html>', '', html, flags=re.I)

        if len(html) == 0:
            html = '<html></html>'

        # we ignore the case where html might be an XML encoded HTML, like <?xml version="1.0" encoding="UTF-8" ?>
        # which will cause lxml.html.fromstring ValueError exception
        # for that case, we MUST use
        # dom = lxml.html.fromstring(html, parser=lxml.html.HTMLParser(remove_comments=True))
        dom = lxml.html.fromstring(html.decode(encoding, 'ignore'), parser=lxml.html.HTMLParser(remove_comments=True))
        return dom, encoding

    def __open_url(self, url):
        cached = self.cache.get(url)
        if cached is not None:
            return cached

        if self.__is_bad_url(url):
            print >> sys.stderr, '  badurl:', encode_string(url)
            return {'data': '~'}

        try:
            # print '  urlopen#2 for', url
            f = urllib2.urlopen(create_request(url, self.url if url != self.url else None), timeout=15)
            data = f.read()
            self.stat.rm_error_url(url.split('?')[0].split('#')[0])
        except Exception, e:
            print >> sys.stderr, '  urlopen {0} failed: {1}'.format(encode_string(url), e.__str__())
            self.stat.add_error_url(url.split('?')[0].split('#')[0])
            self.success = False
            return {'data': '~'}

        encoding = None
        if 'content-type' in f.headers.dict and f.headers.dict['content-type'].count('charset=') == 1:
            encoding = re.findall(r'charset=(.*)', f.headers.dict['content-type'])[0].lower()

        self.cache.set(url, data, get_expire(f), encoding.lower() if encoding is not None else None)
        if data[:6] == '\x1f\x8b\x08\x00\x00\x00':
            data = gzip.GzipFile(fileobj=cStringIO.StringIO(data)).read()

        return {'data': data, 'encoding': encoding}

    def __is_bad_url(self, url):
        walled = ('facebook.com', 'facebook.net', 'twitter.com', 'youtube.com', 'flickr.com')
        domain = urlparse(url).netloc
        if any([(domain == _) or domain.endswith('.' + _) for _ in walled]):
            return True
        url_errors = self.stat.get_error_urls()
        return len([_ for _ in url_errors if _['url'] == url.split('?')[0].split('#')[0] and _['errors'] > 3]) > 0

    def __prune_dom(self, dom, exec_js, encoding):
        # preprocess
        if self.url.startswith('http://blog.sina.com.cn/'):
            for img in dom.xpath('.//img[@real_src]'):
                img.set('src', img.get('real_src'))
                del img.attrib['real_src']
        elif self.url.startswith('http://bbs.qyer.com/'):
            for img in dom.xpath('.//img[@file]'):
                img.set('src', img.get('file'))
                del img.attrib['file']

        self.__remove_useless(dom, exec_js)
        dom.rewrite_links(lambda l: reduce(lambda r, x: r.replace(x[0], x[1]), [('/../', '/'), ('\n', ''), ('\r', '')], urljoin(self.url, l)))
        if exec_js:
            if js_engine is None:
                [_.drop_tree() for _ in dom.findall(".//script")]
            else:
                js_engine.enter_page()
                for js in dom.findall(".//script"):
                    self.__exec_js(js, encoding)

    @staticmethod
    def __remove_useless(dom, exec_js):
        # we can't remove FORM 'cause some forums use it to surround the text...
        useless_eles = []
        for _ in dom.iter():
            if _.tag in ['applet', 'base', 'basefont', 'bgsound', 'button', 'embed', 'frame', 'frameset', 'iframe', 'input', 'isindex', 'label', 'marquee', 'menu', 'object', 'optgroup', 'option', 'param', 'select', 'style', 'textarea']:
                useless_eles.append(_)
            elif _.tag not in ['a', 'abbr', 'acronym', 'address', 'area', 'b', 'bdo', 'big', 'blockquote', 'body', 'br', 'caption', 'center', 'cite', 'code', 'col', 'colgroup', 'comment', 'dd', 'del', 'dfn', 'dir', 'div', 'dl', 'dt', 'em', 'fieldset', 'font', 'form', 'head', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'hr', 'html', 'i', 'img', 'ins', 'kbd', 'legend', 'li', 'listing', 'map', 'meta', 'nobr', 'noframes', 'noscript', 'ol', 'p', 'pre', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'small', 'span', 'strike', 'strong', 'sub', 'sup', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'title', 'tr', 'tt', 'u', 'ul', 'var', 'wbr', 'xml', 'xmp']:
                _.tag = 'div'
            elif _.tag in ['span'] and _.get('onclick') is not None:
                useless_eles.append(_)
            elif _.tag == 'a' and (_.get('href') is None or _.get('href').startswith('javascript:')):
                useless_eles.append(_)
            elif _.tag == 'img' and 'src' not in _.keys():
                useless_eles.append(_)
            elif 'style' in _.keys() and (re.match(r'.*[; ]?visibility *: *hidden[; ]?.*', _.get('style')) or re.match(r'.*[; ]?display *: *none[; ]?.*', _.get('style'))):
                useless_eles.append(_)
            elif not exec_js and _.tag == 'script':
                useless_eles.append(_)
            elif exec_js and _.tag == 'noscript':
                useless_eles.append(_)

        for _ in [e for e in useless_eles if e.getparent() is not None]:
            _.drop_tree()

    def __exec_js(self, js, encoding):
        text = js.text
        if js.get('src') is not None:
            text = self.__open_url(js.get('src'))['data']

            if js.get('charset'):
                encoding = js.get('charset')

        if text is not None and text.count("document.write") > 0:
            text = js_engine.exec_js(text, encoding)
            if text.lower().count("<script") == 0 and not text.lower().startswith("<a "):
                last_sibling = js
                for frag in lxml.html.fragments_fromstring(text):
                    if isinstance(frag, lxml.html.HtmlElement):
                        last_sibling.addnext(frag)
                        self.__remove_useless(frag, True)
                        frag.rewrite_links(lambda l: urljoin(self.url, l).replace('/../', '/').replace('\n', '').replace('\r', ''))
                        last_sibling = frag
                    elif isinstance(frag, str) or isinstance(frag, unicode):
                        p = lxml.html.HtmlElement()
                        p.tag = 'p'
                        p.text = frag
                        last_sibling.addnext(p)
                        last_sibling = p
        js.drop_tree()

    @staticmethod
    def __is_decoration_tag(tag):
        return tag in ["p", "br", "font", "b", "i", "u", "center", "big", "small", "strong", "strike", "sub", "sup", "code", "em", "abbr", "acronym", "address", "blockquote", "cite", "dfn", "samp", "kdb", "var", "del", "ins", "ruby", "rt", "rp", "h1", "h2", "h3", "h4", "h5", "h6", "wbr"]

    # in fact we can use ele.text/ele.text_content() to get the innertext, but the problem is image
    def __build_stat_tree(self, ele):
        tags = 0
        total = 0
        if ele.tag == 'img':
            total = self.__calc_img_text_length(ele)
        else:
            for c in ele.iterchildren():
                ctags, ctotal = self.__build_stat_tree(c)
                tags += ctags
                total += ctotal
            total += self.__get_text_length(ele.text)

        ele.set('_te_purelen', str(total - int(ele.xpath('sum(.//a[not(.//img)]/@_te_textlen | .//a[.//img/@_te_textlen<105]/@_te_textlen)'))))
        ele.set('_te_textlen', str(total))
        ele.set('_te_tags', str(tags))
        self.__calc_density(ele)
        if not self.__is_decoration_tag(ele.tag):
            tags += 1
        return tags, total + self.__get_text_length(ele.tail)

    def __get_text_length(self, text):
        if text is not None:
            s = text.replace("\n", "").replace("\r", "").strip()
            for regex in merges:
                s = regex.sub('*', s)
            return len(s)
        return 0

    def __calc_img_text_length(self, img):
        if {'width', 'height'} < set(img.keys()):
            w = int(re.findall('\d+', img.get('width'))[0])
            h = int(re.findall('\d+', img.get('height'))[0])
            return w*h / 650

        pic = None
        data = self.cache.get(img.get('src'))
        if data is not None:
            try:
                pic = Image.open(cStringIO.StringIO(data['data']))
            except:
                return 0

        if img.attrib['src'].startswith('data:image/'):
            m = re.match(r'data:image/[a-zA-Z0-9]+;([a-zA-Z0-9]+),(.+)', img.get('src'))
            if m is not None and m.group(1) == 'base64':
                data = base64.b64decode(m.group(2))
                pic = Image.open(cStringIO.StringIO(data))
            else:
                return 0

        if pic is None:
            try:
                req = create_request(img.get('src'), self.url)
                req.add_header('Range', 'bytes=0-1023')
                # print '  urlopen#3 for', img.get('src')
                f = urllib2.urlopen(req, timeout=3)
                if 206 == f.getcode() or 200 == f.getcode():
                    data = f.read()
                    pic = Image.open(cStringIO.StringIO(data))
                    self.cache.set(img.get('src'), data, get_expire(f))
            except:
                pass

        if pic is None:
            data = self.__open_url(img.get('src'))
            try:
                pic = Image.open(cStringIO.StringIO(data['data']))
            except:
                pass

        return pic.size[0]*pic.size[1]/650 if pic is not None else 0

    @staticmethod
    def __calc_density(ele):
        if int(ele.get('_te_tags')) > 0:
            ele.set('_te_density', str(int(ele.get('_te_purelen'))*1000 / int(ele.get('_te_tags'))))
        else:
            ele.set('_te_density', str(int(ele.get('_te_purelen'))*1000))

    def __remove_element(self, e):
        if e.getparent() is None:
            return
        text_len = int(e.attrib['_te_textlen'])
        pure_len = int(e.attrib['_te_purelen'])
        tags = int(e.attrib['_te_tags'])
        if not self.__is_decoration_tag(e.tag):
            tags += 1
        self.__update_stat_tree(e, tags, text_len, pure_len)
        e.getparent().remove(e)

    def __update_stat_tree(self, leaf, tags_delta, text_delta, pure_text_delta):
        c = leaf
        while c.getparent() is not None and c.getparent().get('_te_tags') is not None:
            p = c.getparent()
            p.set('_te_purelen', str(int(p.attrib['_te_purelen']) - pure_text_delta))
            p.set('_te_tags', str(int(p.attrib['_te_tags']) - tags_delta))
            p.set('_te_textlen', str(int(p.attrib['_te_textlen']) - text_delta))
            self.__calc_density(p)
            c = p

    def __get_textblock_element(self, root, siblings):
        elements = self.__get_textblocks(root)
        ele = elements[0] if len(elements) == 1 else self.__create_element(elements, True)

        to_crops = [_ for _ in ele.xpath('.//*[@_te_textlen>100]') if too_many_links(_) and _.xpath('count(.//a)') > 1]
        to_crops.reverse()
        [self.__remove_element(_) for _ in to_crops if too_many_links(_)]

        for e in [_ for _ in ele.xpath('.//a[@href]') if _.get('href') in [a.get('href') for a in siblings]]:
            temp = e.getparent()
            while temp != ele:
                if len(temp.xpath('.//a[@href]')) == 1:
                    temp = temp.getparent()
                else:
                    if too_many_links(temp):
                        self.__remove_element(temp)
                    break

        for tag in ['li']:
            while len(ele.xpath('.//{0}[@_te_textlen=0]'.format(tag))) > 0:
                p = ele.xpath('.//{0}[@_te_textlen=0]'.format(tag))[0].getparent()
                if max([int(_) for _ in p.xpath('./{0}/@_te_textlen'.format(tag))]) < 50:
                    self.__remove_element(p)
                else:
                    break

        [self.__remove_element(_) for _ in ele.xpath('.//*[@_te_textlen=0]') if len(set(_.keys()) & {'style', 'width', 'height'}) > 0 or not self.__is_decoration_tag(_.tag)]

        for t in ele.xpath('descendant-or-self::body | descendant-or-self::html'):
            t.set('old_tag', t.tag)
            t.tag = 'div'
        return ele

    def __create_element(self, elements, build_tree=False):
        ele = lxml.html.HtmlElement()
        ele.tag = 'div'
        ele.set('_te_textlen', str(sum([int(_.get('_te_textlen')) for _ in elements])))
        ele.set('_te_purelen', str(sum([int(_.get('_te_purelen')) for _ in elements])))
        ele.set('_te_tags', str(sum([int(_.get('_te_tags')) for _ in elements])))
        for _e in elements:
            if build_tree:
                ele.append(_e)
            if not self.__is_decoration_tag(_e.tag):
                ele.attrib['_te_tags'] = str(int(ele.attrib['_te_tags']) + 1)
        self.__calc_density(ele)
        return ele

    def __is_ads(self, img):
        if int(img.attrib['_te_textlen']) == 0:
            return True

        if img.get('width') is not None and img.get('height') is not None:
            w = int(re.findall('\d+', img.attrib['width'])[0])
            h = int(re.findall('\d+', img.attrib['height'])[0])
            if w > 600 and h*4 < w:
                return True

        data = self.__open_url(img.attrib['src'])
        try:
            pic = Image.open(cStringIO.StringIO(data['data']))
            if ('duration' in pic.info.keys() and pic.info['duration'] > 0) or (pic.size[0] > 600 and pic.size[1]*4 < pic.size[0]):
                return True
        except:
            return int(img.attrib['_te_textlen']) < 105

    @staticmethod
    def __remove_attrs(e):
        for k in [_ for _ in e.keys() if _ in ['id', 'class'] or _.startswith(("on", "_te_"))]:
            del(e.attrib[k])

    def __get_textblocks(self, root):
        total = int(root.attrib['_te_purelen'])
        tags = root.findall('.//*') + [root]
        candidates = [{'ele': None, 'density': 0}, {'ele': None, 'density': 0}, {'ele': None, 'density': 0}]
        for t in tags:
            if (t.tag != 'img' and t.xpath('count(ancestor-or-self::a[1])') > 0) or too_many_links(t):
                continue
            density = int(t.attrib['_te_density'])
            if int(t.attrib['_te_purelen'])*4 > total and density > candidates[0]['density'] and len([_ for _ in t.xpath('.//*[@_te_textlen>100]') if too_many_links(_)]) == 0:
                candidates[0]['density'] = density
                candidates[0]['ele'] = t
            if int(t.attrib['_te_purelen']) > 10 and density > candidates[1]['density']:
                candidates[1]['density'] = density
                candidates[1]['ele'] = t
            if int(t.attrib['_te_purelen']) > 10 and density > candidates[2]['density'] and 'class' in t.keys():
                candidates[2]['density'] = density
                candidates[2]['ele'] = t

        ele = candidates[1]['ele'] if candidates[0]['ele'] is None else candidates[0]['ele']
        if ele is None:
            return []

        elements = []
        if candidates[2]['ele'] is not None:
            candi = candidates[2]['ele']
            xpath = u'.//{0}[@class="{1}"][@_te_purelen>{2}]'.format(candi.tag, candi.get('class'), int(candi.attrib['_te_purelen'])/16)
            elements = [_ for _ in root.xpath(xpath) if not too_many_links(_)]
            if sum([int(_.attrib['_te_purelen']) for _ in elements]) >= total*0.8:
                return elements

        c0 = candidates[0]['ele']
        c1 = candidates[1]['ele']
        c2 = candidates[2]['ele']
        real_arr = c2 is not None and root.xpath(u'count(.//{0}[@class="{1}"])'.format(c2.tag, c2.get('class'))) > 9
        skip_cmn = real_arr and c0 is not None and (c0 == c2 or (c1 == c2 and c0 in c1.xpath('ancestor-or-self::*')) and int(c0.attrib['_te_purelen']) < 2*(c1.attrib['_te_purelen']))
        if skip_cmn:
            return elements

        eles_common = self.__match_common(ele, root, elements if real_arr and ele == c2 else [])
        common_len = sum([int(_.attrib['_te_purelen']) for _ in eles_common])
        eleslen = sum([int(_.attrib['_te_purelen']) for _ in elements])

        if len(eles_common) > 0 and (len(eles_common) == 1 or len(list(eles_common[0].getparent())) == len(eles_common)) and (eleslen >= common_len > eleslen/4 or common_len >= eleslen > common_len/4):
            elements = self.__combine(eles_common[0] if len(eles_common) == 1 else eles_common[0].getparent(), root, elements)
        elif eleslen < common_len:
            elements = eles_common

        temp = elements[0] if len(elements) == 1 else self.__create_element(elements)
        if int(temp.attrib['_te_purelen'])*8 < int(root.attrib['_te_purelen']) and c2 is not None:
            elements = [_ for _ in root.xpath('.//{0}[@_te_purelen>{1}]'.format(c2.tag, int(ele.attrib['_te_purelen'])/4)) if _ != ele and not too_many_links(_)]
            elements = self.__combine(ele, root, elements)
        return elements

    def __match_common(self, seed, root, threads):
        if len(threads) > 0:
            noqualify = lambda e, l : len(set(e.xpath(u'.//{0}[@class="{1}"]'.format(threads[0].tag, threads[0].get('class')))) & set(threads)) == 0 and (e not in threads) and int(e.attrib['_te_textlen']) > l and too_many_links(e)
        else:
            noqualify = lambda e, l : int(e.attrib['_te_textlen']) > l and too_many_links(e)
        trunk = seed
        elements = [trunk]
        crop_head = crop_tail = False
        while trunk not in root.xpath('ancestor-or-self::*'):
            elements = [trunk]
            if trunk.getparent().xpath('count(.//a)') == trunk.xpath('count(.//a)'):
                trunk = trunk.getparent()
                continue

            chs = list(trunk.getparent())
            heads = [] if crop_head else [_ for _ in chs if chs.index(_) < chs.index(trunk)]
            tails = [] if crop_tail else [_ for _ in chs if chs.index(_) > chs.index(trunk)]

            if not crop_head:
                crop_head, heads = self.__crop_headlinks(heads)

            if not crop_tail:
                crop_tail, tails = self.__crop_tail_links(tails)

            if trunk.getparent() in root.xpath('ancestor-or-self::*'):
                link_count = sum([int(_.xpath('count(.//a)')) for _ in heads + tails])
                if link_count > 0:
                    break

            chs = heads + elements + tails

            if len(threads) > 0:
                for ch in chs:
                    [self.__remove_element(_) for _ in set(ch.xpath(u'.//{0}[@class="{1}"]'.format(threads[0].tag, threads[0].get('class')))) - set(threads)]

            elements = [_ for _ in chs if not noqualify(_, max(int(trunk.attrib['_te_purelen'])/10, 10))]
            [self.__remove_element(_) for _ in list(trunk.getparent()) if _ not in elements]
            if crop_head and crop_tail:
                break
            trunk = trunk.getparent()

        # remove extra head and tail elements
        if len(elements) > 1 and crop_head and crop_tail:
            _, elements = self.__crop_headlinks(elements, True)
            _, elements = self.__crop_tail_links(elements, True)
        return elements

    def __crop_headlinks(self, heads, strict=False):
        flags = [too_many_links(self.__create_element(heads[:l + 1])) for l in range(len(heads))]
        if not any(flags):
            return False, heads

        last_true = reduce(lambda r, x: flags[r+1:].index(x) + r + 1 if x else r, flags, -1)
        if strict or all(flags):
            i = last_true
        else:
            for i in range(last_true, -1, -1):
                if too_many_links(self.__create_element(heads[i:])):
                    break
        heads = heads[i+1:]
        return len(flags) != len(heads), heads

    def __crop_tail_links(self, tails, strict=False):
        flags = [too_many_links(self.__create_element(tails[-(l + 1):])) for l in range(len(tails))]
        if not any(flags):
            return False, tails

        if strict:
            flags.reverse()
            i = reduce(lambda r, x: flags[r+1:].index(x) + r + 1 if x else r, flags, -1)
        else:
            for i in range(flags.index(True), len(flags)):
                if too_many_links(self.__create_element(tails[:i+1])):
                    break
        tails = tails[:i]
        return len(flags) != len(tails), tails

    def __combine(self, trunk, root, eles):
        if len(eles) == len(set(trunk.xpath('.//{0}[@class="{1}"]'.format(eles[0].tag, eles[0].get('class')))) & set(eles)):
            return [trunk]

        xpath = u'.//{0}[@class="{1}"]'.format(eles[0].tag, eles[0].get('class'))
        cmn_root = trunk
        # trunk and eles share at least one parent: BODY, so the loop will almost sure succeed
        while cmn_root not in root.xpath('ancestor-or-self::*'):
            if len(eles) == len(set(cmn_root.xpath(xpath)) & set(eles)):
                break
            cmn_root = cmn_root.getparent()

        if cmn_root.getparent() in root.xpath('ancestor-or-self::*'):
            if int(trunk.get('_te_purelen')) > sum([int(_.get('_te_purelen')) for _ in eles]):
                return [trunk]
            else:
                return eles

        family = set(trunk.xpath('ancestor-or-self::*'))
        for ele in eles:
            family |= set(ele.xpath('ancestor-or-self::*'))

        for ele in eles:
            temp = ele
            while temp != cmn_root:
                branches = [_ for _ in list(temp.getparent()) if _ != temp]
                [self.__remove_element(c) for c in branches if c not in family]
                temp = temp.getparent()
        return [cmn_root]

    def __get_siblings(self, dom):
        parts = urlparse(self.url)
        url = parts.scheme + '://' + parts.netloc + parts.path + ('?' + parts.query if len(parts.query) > 0 else '')
        links = []
        texts = dict()
        for a in dom.xpath('.//a[starts-with(@href, "{0}://{1}{2}")]'.format(parts.scheme, parts.netloc, os.path.dirname(parts.path))):
            this_parts = urlparse(a.get('href'))
            if this_parts.path.count('/') != parts.path.count('/') or url.count('?') != a.get('href').count('?'):
                continue

            this_url = this_parts.scheme + '://' + this_parts.netloc + this_parts.path + ('?' + this_parts.query if len(this_parts.query) > 0 else '')
            if url == this_url:
                continue

            guess = False
            if self.url.count('?') == 1:
                if this_parts.path != parts.path:
                    continue
                if set(parse_qs(this_parts.query).keys()) != set(parse_qs(parts.query).keys()):
                    diff1 = len(set(parse_qs(this_parts.query).keys()) - set(parse_qs(parts.query).keys()))
                    diff2 = len(set(parse_qs(parts.query).keys()) - set(parse_qs(this_parts.query).keys()))
                    if (diff1 == 0 and diff2 == 1) or (diff1 == 1 and diff2 == 0):
                        guess = True
                    else:
                        continue
            elif this_parts.path.endswith('/') != parts.path.endswith('/'):
                continue

            if this_url not in texts or len(texts[this_url]['text']) == 0 or len(texts[this_url]['text']) > len(a.text_content().strip()) > 0:
                apos = len(links) if this_url not in texts else texts[this_url]['apos']
                if apos == len(links):
                    links.append(a)
                else:
                    links[apos] = a
                texts[this_url] = {'text': a.text_content().strip(), 'apos': apos, 'guess': guess}

        if len(links) == 0:
            for a in dom.xpath('.//a[starts-with(@href, "{0}://{1}/")]'.format(parts.scheme, parts.netloc)):
                if a.text_content().strip().isdigit() and a.get('href') not in [_.get('href') for _ in links]:
                    if a.text_content().strip() in [_.text_content().strip() for _ in links]:
                        links = []
                        break
                    else:
                        links.append(a)

        if len(texts) > len([_ for _ in texts.values() if not _['guess']]) > 0:
            links = [_ for _ in links if urlparse(_.get('href')).query.count('&') == parts.query.count('&')]

        if len(links) > 12 and (os.path.basename(parts.path).count('-') > 0 or os.path.basename(parts.path).count('_') > 0):
            sep = '-' if os.path.basename(parts.path).count('-') > 0 else '_'
            pieces = os.path.basename(parts.path).split(sep)
            i = 1
            while i <= len(pieces) and len(links) > 12:
                pref = parts.scheme + '://' + parts.netloc + os.path.dirname(parts.path) + sep.join(pieces[:i])
                if i == 1 and len([_ for _ in links if _.get('href').startswith(pref)]) == 0:
                    break
                links = [_ for _ in links if _.get('href').startswith(pref)]
                i += 1

        # it's really related links, not real continuous links
        if len(links) > 12 or sum([len(a.text_content().strip()) for a in links]) > 80:
            numbers = reduce(lambda r, a: r + 1 if a.text_content().strip().isdigit() else r, links, 0)
            if numbers > len(links)/2 > 0:
                links = [_ for _ in links if _.text_content().strip().isdigit()]
            else:
                return []

        for a in links:
            if len(a.xpath('.//img')) == 0:
                text = a.text_content().strip()
                href = a.get('href')
                a.clear()
                a.set('href', href)
                a.text = text
            else:
                a.tail = ''

        return links