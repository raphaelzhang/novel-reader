import sys
import os

# ported from http://fayaa.com/code/view/35/full/
def print_nt(foreground, *kw, **kwargs):
    from ctypes import windll, Structure, c_short, c_uint, byref

    # +8 means highlight
    cc_map = {
        'black': 0,
        'darkblue': 1,
        'darkgreen': 2,
        'darkcyan': 3,
        'darkred': 4,
        'darkmagenta': 5,
        'brown': 6, #dark yellow
        'darkwhite': 7,
        'blue': 9,
        'green': 10,
        'cyan': 11,
        'red': 12,
        'magenta': 13,
        'yellow': 14,
        'white': 15,
        }

    STD_OUTPUT_HANDLE = -11
    STD_ERROR_HANDLE = -12

    class COORD(Structure):
        _fields_ = [('X', c_short), ('Y', c_short)]

    class SMALL_RECT(Structure):
        _fields_ = [('Left', c_short),
                   ('Top', c_short),
                   ('Right', c_short),
                   ('Bottom', c_short),
                  ]

    class CONSOLE_SCREEN_BUFFER_INFO(Structure):
        _fields_ = [('dwSize', COORD),
                   ('dwCursorPosition', COORD),
                   ('wAttributes', c_uint),
                   ('srWindow', SMALL_RECT),
                   ('dwMaximumWindowSize', COORD),
                  ]
    
    if foreground in cc_map:
        GetStdHandle = windll.kernel32.GetStdHandle
        hconsole = GetStdHandle(STD_ERROR_HANDLE) if 'stderr' in kwargs and kwargs['stderr'] else GetStdHandle(STD_OUTPUT_HANDLE)
        scrinfo = CONSOLE_SCREEN_BUFFER_INFO()
        windll.kernel32.GetConsoleScreenBufferInfo(hconsole, byref(scrinfo))
        oldcolor = scrinfo.wAttributes
        windll.kernel32.SetConsoleTextAttribute(hconsole, cc_map[foreground])

    output = sys.stderr if 'stderr' in kwargs and kwargs['stderr'] else sys.stdout
    for t in kw:
        print >> output, t,
    if 'nocrlf' not in kwargs or not kwargs['nocrlf']:
        print >> output

    if foreground in cc_map:
        windll.kernel32.SetConsoleTextAttribute(hconsole, oldcolor)


# code source: http://nick.workao.org/index.php/linux/692
def print_inx(foreground, *kw, **kwargs):
    cc_map = {
        'black': '30',
        'darkred': '31',
        'darkgreen': '32',
        'brown': '33', #dark yellow
        'darkblue': '34',
        'darkmagenta': '35',
        'darkcyan': '36',
        'darkwhite': '37',
        'red': '1;31',
        'green': '1;32',
        'yellow': '1;33',
        'blue': '1;34',
        'magenta': '1;35',
        'cyan': '1;36',
        'white': '1;37',
    }

    output = sys.stderr if 'stderr' in kwargs and kwargs['stderr'] else sys.stdout
    if foreground in cc_map:
        for t in kw:
            print >> output, '\033[' + cc_map[foreground] + 'm{0}\033[0m'.format(t),
    else:
        for t in kw:
            print >> output, t,

    if 'nocrlf' not in kwargs or not kwargs['nocrlf']:
        print >> output


def print_colorful(foreground, *kw, **kwargs):
    try:
        if foreground == 'darkyellow':
            foreground = 'brown'

        if os.name == 'nt':
            print_nt(foreground, *kw, **kwargs)
        else:
            print_inx(foreground, *kw, **kwargs)
    except:
        output = sys.stderr if 'stderr' in kwargs and kwargs['stderr'] else sys.stdout
        for t in kw:
            print >> output, t,
        if 'nocrlf' not in kwargs or not kwargs['nocrlf']:
            print >> output


if __name__ == '__main__':
    for c in ['red', 'green', 'cyan', 'blue', 'yellow', 'magenta', 'white']:
        print_colorful(c, 'test for ' + c, nocrlf = True)
        print_colorful('dark' + c, '& for dark' + c)
