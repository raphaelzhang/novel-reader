# -*- coding: utf-8 -*-

import sys
import os
from datetime import datetime
from urlparse import urlparse
import argparse
import cProfile
import pstats
import traceback

import lxml.html

from utils import save_file, encode_string
from fscache import FsCache
from doc_builder import DocBuilder
from extractor import Extractor


def get_args():
    common = [
        "http://news.163.com/11/0517/03/747PJ7KU00014AED.html", 
        "http://news.ifeng.com/society/2/detail_2011_05/17/6441271_0.shtml", 
        "http://news.ifeng.com/society/2/detail_2011_05/17/6441271_1.shtml", 
        "http://bbs.eduu.com/thread-818767-1-1.html", 
        "http://mil.news.sohu.com/20110517/n307637039.shtml",
        "http://www.hudong.com/wiki/%E5%AE%8B%E5%A4%AA%E7%A5%96?prd=fenleishequ_jiaodiantuijian_zuocitiao",
        "http://wvw.ranwen.com/html/38/38247/7508015.html", 
        "http://www.mohuandi.com/html/30/30851/5495448.html", 
        "http://www.1616.net/jd/misc/about_us.htm", 
        "http://www.qidian.com/BookReader/1943704,32697535.aspx", 
        "http://ent.huanqiu.com/star/world/2011-05/1711332_2.html", 
        "http://ent.huanqiu.com/moive/world/2011-05/1706744.html", 
        "http://www.reuters.com/article/2009/10/07/amazon-shares-idUSN0738002820091007",
        "http://news.163.com/09/0513/18/597E51E2000120GR.html",
        "http://www.reuters.com/article/2011/05/27/us-ladygaga-idUSTRE74P0KC20110527", 
        "http://stackoverflow.com/questions/2720484/use-twisteds-getpage-as-urlopen",
        "http://dzh.mop.com/shzt/20110615/0/7O338gI26d159dFO.shtml",
        "http://www.tianya.cn/publicforum/content/funinfo/1/2668621.shtml",
        "http://zhidao.baidu.com/browse/172",
    ]
    blogs = [
        "http://blog.sina.com.cn/s/blog_5e1df4580102drg9.html?tj=1",
        "http://www.ruanyifeng.com/blog/2011/05/final_ruling_of_the_case_of_gold-farming_bot.html",
        "http://obmem.info/?p=521", 
        "http://mindhacks.cn/2011/01/23/escape-from-your-shawshank-4/", 
        "http://www.wangxiaofeng.net/?p=7548",
        "http://www.dbanotes.net/arch/quora_tech.html",
        "http://www.lixiaolai.com/index.php/archives/10620.html",
        "http://www.guwendong.com/post/2011/recsys-will-explode.html",        
    ]
    forums = [
        "http://bbs.55bbs.com/thread-5556351-1-1.html",
        "http://www.go2eu.com/bbs/viewthread.php?tid=419015&extra=page%3D1%26amp%3Bfilter%3Dtype%26amp%3Btypeid%3D54",
        "http://bbs.55bbs.com/thread-5561989-1-1.html",
        "http://bbs.hualongxiang.com/read-htm-tid-7762124.html", 
    ]
    extra = [
        "http://www.mohuandi.com/html/18/18183/3005715.html",
        "http://myphonedeals.co.uk/blog/33-the-smartphone-os-complete-comparison-chart",
        "http://www.talkandroid.com/71416-have-you-ever-wanted-to-use-your-android-device-as-a-universal-remote-youre-in-luck/",
        "http://www.infoq.com/news/2011/10/steve-jobs-contribution",
        "http://www.dianping.com/shop/2972311?KID=79917",
        "https://market.android.com/details?id=com.rovio.angrybirds",
        "http://book.douban.com/subject/1230487/",
        "http://www.dukankan.com/Html/9979/4139728.shtml",
    ]
    
    parser = argparse.ArgumentParser(description="Download, extract and save web page content into local files")
    parser.add_argument('urls', metavar='URLS', nargs='*', help='urls of web pages to download and process')
    parser.add_argument('--execjs', '-j', dest='execjs', action='store_true', help='execute javascript(s) in web pages')
    parser.add_argument('--nocache', '-n', dest='nocache', action='store_true', help='remove expired cached web content automatically')
    parser.add_argument('--list', '-l', dest='listcache', action='store_true', help='list all cached urls (except images & css(es))')
    parser.add_argument('--folder', '-f', dest='folder', metavar='FOLDER', help='list all offline files in a folder')
    parser.add_argument('--output', '-o', dest='output', default="./output/", help="output folder to save all the offline files (default: %(default)s)")
    parser.add_argument('--prof', '-p', dest='profile', action='store_true', help="profile this program")
    parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='debug extraction process')
    
    args = parser.parse_args()

    if args.urls is None:
        args.urls = common + blogs + forums + extra
    
    return args.urls, args.execjs, args.nocache, args.listcache, args.folder, args.output, args.profile, args.debug


def debug_cb(step, dom, url, encoding):
    print '=' * 80
    print step, url
    for l in dom.text_content().split('\n'):
        if len(l.strip()) > 0:
            print l.rstrip()
    raw_input('step: {0}, url: {1}'.format(step, url))

def main(urls, exec_js, nocache, folder, debug):
    cache = FsCache(always_cache=not nocache)

    for i in range(len(urls)):
        url = urls[i]

        print '[{0}/{1} {2}]: {3}'.format(i+1, len(urls), datetime.now().strftime('%H:%M:%S'), url)
        parts = urlparse(url)
        extractor = Extractor(url, cache=cache)

        try:
            text = extractor.get_text(parts.netloc.lower() == 'www.qidian.com', debug_callback=debug_cb if debug else None)
            builder = DocBuilder(url, text['title'], text['html'], text['links'])
            html = builder.build_html(u'浏览原网页', u'打印')
            save_file(folder, '{0}_{1}.html'.format(i+1, parts.netloc), html.encode('utf-8'))
        except:
            traceback.print_exc(file=sys.stderr)


def print_info(index, folder, fn):
    if not os.path.isfile(os.path.join(folder, fn)) or not fn.endswith(('.html', '.htm')):
        return False

    with open(os.path.join(folder, fn)) as fd:
        content = fd.read()

    content = content.decode('utf-8', 'ignore')
    if len(content) == 0:
        return False

    dom = lxml.html.fromstring(content, parser=lxml.html.HTMLParser(remove_comments=True))
    titles = dom.xpath('.//title[string-length(text()) > 0]')
    title = titles[0].text if len(titles) > 0 else ''
    links = dom.xpath('.//a[@target="_blank"][@href]/@href')
    link = links[0] if len(links) > 0 else ''
    texts = dom.xpath('.//div[@id="content"]')
    text = texts[0].text_content()[:60] if len(texts) > 0 else ''
    print '{0}: {1}'.format(index, fn)
    print '\t', title.split('-')[0].strip()
    print '\t', link
    print '\t', encode_string(text.strip())
    return True


if __name__ == '__main__':
    urls, exec_js, nocache, list_cache, folder, output, profile, debug = get_args()
    if list_cache:
        cache = FsCache(always_cache=True)
        rows, reqs, hits, _ = cache.get_statistics()
        for r in rows:
            if r['url'].lower().endswith(('.png', '.jpg', '.gif', '.css')):
                continue

            print r
        exit()

    if folder is not None:
        if not os.path.isdir(folder):
            print >> sys.stderr, '{0} does not exist'.format(folder)
            exit(1)

        count = 0
        for f in os.listdir(folder):
            if print_info(count, folder, f):
                count += 1
        exit()

    if profile:
        prof = cProfile.Profile()
        prof.enable()
        main(urls, exec_js, nocache, output, debug)
        prof.disable()
        pstats.Stats(prof).strip_dirs().sort_stats('cumulative').print_stats(25)
        # p.strip_dirs().sort_stats(-1).print_stats()
    else:
        main(urls, exec_js, nocache, output, debug)
