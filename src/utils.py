import os
import re
import datetime
import urllib2
import rfc822
import locale

import lxml.html


def encode_string(s):
    return s.encode(locale.getdefaultlocale()[1], 'ignore')


def create_request(url, referer=None):
    req = urllib2.Request(
        urllib2.quote(url.split('#')[0].encode('utf8'), safe="%/:=&?~#+!$,;'@()*[]"),
        headers={"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                 "Accept-Charset": "GBK,utf-8;q=0.7,*;q=0.3",
                 "Accept-Encoding": "gzip",
                 "Accept-Language": "zh-CN,zh;q=0.8",
                 "Cache-Control": "max-age=0",
                 "Connection": "keep-alive",
                 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36",
                 })

    if referer is not None:
        req.add_header('Referer', referer)
    return req


def get_expire(f):
    try:
        t = rfc822.parsedate(f.info().get('Expires'))
        expire = datetime.datetime(t[0], t[1], t[2], t[3], t[4], t[5]) + datetime.timedelta(hours=8)
    except:
        expire = datetime.datetime(1900, 1, 1, tzinfo=None)

    if f.info().get('Cache-Control') is not None:
        m = re.match(r'max-age=(\d+)', f.info().get('Cache-Control'))
        if m is not None:
            expire = datetime.datetime.now() + datetime.timedelta(seconds=int(m.group(1)))

    return expire


def save_file(folder, name, data):
    if os.path.isfile(folder):
        os.remove(folder)
    if not os.path.isdir(folder):
        os.makedirs(folder)
    f = open(os.path.join(folder, name), 'wb')
    f.write(data)
    f.close()


def get_encoding(html):
    dom = lxml.html.fromstring(html.decode('utf8', 'ignore'), parser=lxml.html.HTMLParser(remove_comments=True))
    encs = dom.xpath('.//head/meta[@charset]/@charset')
    encs += [re.findall(r'charset=(.*)', _.get('content'))[0] for _ in dom.xpath('.//head/meta[@http-equiv][@content]')
             if _.get('http-equiv').lower() == "content-type" and _.get('content').count('charset=') == 1]
    encs = set([_.lower() for _ in encs])

    if {'gb2312', 'gbk'} <= encs:
        encs.remove('gb2312')
    if {'gb2312'} == encs:
        encs = {'gbk'}

    if len(encs) == 1:
        return encs.pop()

    # no encoding or multiple encoding(for web-cache sites)
    try:
        import chardet
        return chardet.detect(html)['encoding']
    except ImportError, e:
        raise e


js_engine = None
try:
    import win32com.server.util
    import win32com.client


    class Win32Document:
        _public_methods_ = ['write']

        def __init__(self):
            self.output = ''

        def write(self, s):
            self.output += s

        def read(self):
            return self.output

        def clear(self):
            self.output = ''


    class Win32JsEngine:
        def __init__(self):
            self.ax = None
            self.engine_impl = None

        def enter_page(self):
            self.ax = Win32Document()
            self.engine_impl = win32com.client.Dispatch('MSScriptControl.ScriptControl')
            self.engine_impl.language = 'JavaScript'
            self.engine_impl.allowUI = False
            self.engine_impl.addObject('document', win32com.server.util.wrap(self.ax))

        def exec_js(self, js, encoding):
            self.ax.clear()
            try:
                self.engine_impl.eval(js.decode(encoding, 'ignore'))
            except:
                self.ax.clear()
            return self.ax.read()


    js_engine = Win32JsEngine()
except ImportError:
    pass

if js_engine is None:
    try:
        import PyV8


        class V8Document(PyV8.JSClass):
            output = ''

            def write(self, s):
                self.output += s

            def read(self):
                return self.output

            def clear(self):
                self.output = ''


        class Global(PyV8.JSClass):
            def __init__(self):
                self.document = V8Document()


        class V8JsEngine:
            def __init__(self):
                self.glob = None
                self.ctx = None

            def enter_page(self):
                self.glob = Global()
                self.ctx = PyV8.JSContext(self.glob)
                self.ctx.enter()

            def exec_js(self, js, encoding):
                self.glob.document.clear()
                try:
                    self.ctx.eval(js.decode(encoding, 'ignore'))  # for linux
                except:
                    self.glob.document.clear()
                    try:
                        self.ctx.eval(js.decode(encoding, 'ignore').encode('utf-8'))  # for windows
                    except:
                        self.glob.document.clear()
                return self.glob.document.read().decode('utf-8')


        js_engine = V8JsEngine()
    except ImportError:
        pass
